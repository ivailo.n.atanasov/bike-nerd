enum UserAd { Ads, Favorite }

class UserAdType {
  final UserAd userAd;

  UserAdType(this.userAd);

  String getName() {
    return userAd == UserAd.Ads ? 'ads' : 'favorite';
  }
}
