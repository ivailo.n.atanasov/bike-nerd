import 'package:auto_size_text/auto_size_text.dart';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:bikenerd/ads/screen/ad_details_screen.dart';
import 'package:bikenerd/more/model/user_ad_type.dart';
import 'package:bikenerd/ressources/strings.dart';
import 'package:bikenerd/utils/firebase_api.dart';
import 'package:bikenerd/utils/ui_utils.dart';
import 'package:bikenerd/utils/utils.dart';
import 'package:bikenerd/utils/validator.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class UserAdsScreen extends StatefulWidget {
  @override
  _UserAdsScreenState createState() => _UserAdsScreenState();
}

class _UserAdsScreenState extends State<UserAdsScreen> {
  final _listKey = GlobalKey<AnimatedListState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: UiUtils.appBar(context, Strings.moreMyAds),
      body: FutureBuilder(
        future: FirebaseApi.getUserAds(UserAdType(UserAd.Ads)),
        builder: (context, snapshot) {
          if (snapshot.connectionState != ConnectionState.done) {
            return Center(child: CircularProgressIndicator());
          }

          final userAds = snapshot.data as List<DocumentSnapshot>;
          if (userAds.length == 0) {
            return Center(child: Text(Strings.userAdsNoAds));
          }
          return AnimatedList(
              key: _listKey,
              initialItemCount: userAds.length,
              itemBuilder: (context, index, animation) {
                final heroTag = 'picture-$index';
                return UserAdListItem(
                    userAds[index],
                    heroTag,
                    () => _removeItem(context, index, heroTag, userAds),
                    () => _showChangePriceDialog(userAds[index]));
              });
        },
      ),
    );
  }

  _showChangePriceDialog(DocumentSnapshot documentSnapshot) {
    showDialog(
        barrierDismissible: true,
        context: context,
        builder: (_) => ChangePriceWidget(documentSnapshot)).then((isApproved) {
      if (isApproved != null && isApproved) {
        Utils.showSuccessSnackBar(context, Strings.userAdsNewPriceSuccess);
        setState(() {});
      }
    });
  }

  _removeItem(BuildContext context, int index, String heroTag,
      List<DocumentSnapshot> userAds) {
    final removedItem = userAds.removeAt(index);
    _listKey.currentState.removeItem(index, (context, animation) {
      return SlideTransition(
          position: animation
              .drive(Tween(begin: Offset(2, 0.0), end: Offset(0.0, 0.0))),
          child: UserAdListItem(removedItem, null, null, null));
    });
    FirebaseApi.removeUserAd(context, removedItem);
  }
}

class ChangePriceWidget extends StatelessWidget {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController controller = TextEditingController();
  final DocumentSnapshot documentSnapshot;

  ChangePriceWidget(this.documentSnapshot);

  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(top: 16, left: 16, right: 16),
          child: Form(
            key: _formKey,
            child: Column(
              children: <Widget>[
                Text(
                  '${Strings.userAdsChangePrice} ${Utils.getDisplayedPrice(documentSnapshot['price'])}',
                  textAlign: TextAlign.start,
                ),
                SizedBox(
                  height: 16,
                ),
                TextFormField(
                  controller: controller,
                  maxLength: 6,
                  textInputAction: TextInputAction.done,
                  keyboardType: TextInputType.number,
                  validator: Validator.isPriceValid,
                  decoration: InputDecoration(
                    labelText: Strings.userAdsNewPrice,
                    icon: Icon(Icons.euro_symbol),
                  ),
                ),
                ButtonBar(
                  children: <Widget>[
                    FlatButton(
                        child: Text(
                          Strings.generalCancel,
                          style:
                              TextStyle(color: Theme.of(context).accentColor),
                        ),
                        onPressed: () {
                          Navigator.of(context).pop(false);
                        }),
                    FlatButton(
                        child: Text(Strings.generalConfirm,
                            style: TextStyle(
                                color: Theme.of(context).accentColor)),
                        onPressed: () {
                          if (_formKey.currentState.validate()) {
                            final newPrice = double.parse(controller.text);
                            FirebaseApi.updateDocumentValue(
                                'price', newPrice, documentSnapshot);
                            Navigator.of(context).pop(true);
                          }
                        }),
                  ],
                )
              ],
            ),
          ),
        ),
      ],
    );
  }
}

class UserAdListItem extends StatelessWidget {
  final DocumentSnapshot documentSnapshot;
  final String heroTag;
  final Function removeItemFunction;
  final Function changeItemPriceFunction;

  UserAdListItem(this.documentSnapshot, this.heroTag, this.removeItemFunction,
      this.changeItemPriceFunction);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => _openAdDetails(context),
      child: Card(
        elevation: 2,
        child: Padding(
          padding: const EdgeInsets.all(12),
          child: Row(
            children: <Widget>[
              Hero(
                tag: heroTag == null ? "" : heroTag,
                child: CircleAvatar(
                  backgroundImage: NetworkImage(
                      documentSnapshot['ad_pictures']['picture_1']['url']),
                  backgroundColor: Colors.transparent,
                ),
              ),
              SizedBox(
                width: 12,
              ),
              Expanded(
                  child: AutoSizeText(
                documentSnapshot['title'],
                softWrap: true,
                overflow: TextOverflow.fade,
              )),
              IconButton(
                icon: Icon(
                  Icons.euro_symbol,
                  color: Theme.of(context).accentColor,
                ),
                onPressed: changeItemPriceFunction,
              ),
              IconButton(
                icon: Icon(
                  Icons.delete,
                  color: Colors.red,
                ),
                onPressed: () => _showAlertDialog(context),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _showAlertDialog(BuildContext context) {
    AwesomeDialog(
      context: context,
      dialogType: DialogType.INFO,
      animType: AnimType.BOTTOMSLIDE,
      title: Strings.generalWarning,
      desc: Strings.userAdsDeleteAlert,
      btnCancelOnPress: () {},
      btnOkOnPress: removeItemFunction,
    )..show();
  }

  _openAdDetails(BuildContext context) {
    Navigator.of(context).pushNamed(AdDetailsScreen.routeName,
        arguments: DetailsScreenArguments(documentSnapshot, heroTag));
  }
}
