import 'package:bikenerd/ads/screen/ad_details_screen.dart';
import 'package:bikenerd/ads/widget/ad_list_item.dart';
import 'package:bikenerd/more/model/user_ad_type.dart';
import 'package:bikenerd/ressources/strings.dart';
import 'package:bikenerd/utils/firebase_api.dart';
import 'package:bikenerd/utils/ui_utils.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class UserFavoriteAdsScreen extends StatefulWidget {
  @override
  _UserFavoriteAdsScreenState createState() => _UserFavoriteAdsScreenState();
}

class _UserFavoriteAdsScreenState extends State<UserFavoriteAdsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: UiUtils.appBar(context, Strings.moreFavorite),
      body: FutureBuilder(
        future: FirebaseApi.getUserAds(UserAdType(UserAd.Favorite)),
        builder: (context, snapshot) {
          if (snapshot.connectionState != ConnectionState.done) {
            return Center(child: CircularProgressIndicator());
          }
          final userAds = snapshot.data as List<DocumentSnapshot>;
          if (userAds.length == 0) {
            return Center(child: Text(Strings.userAdsNoFavorite));
          }
          return GridView.builder(
              itemCount: userAds.length,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                childAspectRatio: 2 / 2,
                mainAxisSpacing: 6,
                crossAxisSpacing: 2,
              ),
              itemBuilder: (context, index) {
                final document = userAds[index];
                final String heroTag = 'picture-$index';
                return GestureDetector(
                  onTap: () {
                    Navigator.of(context)
                        .pushNamed(AdDetailsScreen.routeName,
                            arguments:
                                DetailsScreenArguments(document, heroTag))
                        .then((value) {
                      setState(() {});
                    });
                  },
                  child: AdListItem(
                      document['title'],
                      (document['ad_pictures']['picture_1']['url']),
                      document['price'],
                      heroTag,
                      FirebaseApi.getAdTypeFromInt(document['ad_type'])),
                );
              });
        },
      ),
    );
  }
}
