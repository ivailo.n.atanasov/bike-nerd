import 'package:bikenerd/more/screen/user_ads_screen.dart';
import 'package:bikenerd/more/screen/user_favorite_ads_screen.dart';
import 'package:bikenerd/ressources/strings.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class MoreMainScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: const EdgeInsets.all(8),
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            SizedBox(height: 64),
            FlatButton(
              child: Text(Strings.moreFavorite, style: _getStyle(context)),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => UserFavoriteAdsScreen()),
                );
              },
            ),
            Divider(),
            FlatButton(
              child: Text(Strings.moreMyAds, style: _getStyle(context)),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => UserAdsScreen()),
                );
              },
            ),
            FlatButton(
              child: Text('My Events', style: _getStyle(context)),
              onPressed: () {},
            ),
            Divider(),
            FlatButton(
                child: Text("About", style: _getStyle(context)),
                onPressed: () => showAboutDialog(
                    context: context,
                    applicationName: 'BikeNerd',
                    applicationLegalese: Strings.moreAboutDialog,
                    applicationVersion: '1.0.0')),
            Divider(),
            FlatButton(
              child: Text(Strings.moreLogout, style: _getStyle(context)),
              onPressed: () {
                FirebaseAuth.instance.signOut();
                Navigator.of(context).pushReplacementNamed('/');
              },
            ),
            FlatButton(
              child: Text('Delete Account', style: _getStyle(context)),
              onPressed: () {},
            ),
          ],
        ),
      ),
    );
  }

  TextStyle _getStyle(BuildContext context) {
    return TextStyle(fontSize: 18);
  }
}
