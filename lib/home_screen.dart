import 'package:bikenerd/ads/model/ad.dart';
import 'package:bikenerd/ads/screen/add_new_ad_screen.dart';
import 'package:bikenerd/events/screen/add_new_event_screen.dart';
import 'package:bikenerd/login/screen/login_screen.dart';
import 'package:bikenerd/more/screen/more_main_screen.dart';
import 'package:bikenerd/ressources/strings.dart';
import 'package:bikenerd/utils/ui_utils.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import 'ads/screen/ads_list_screen.dart';
import 'events/screen/events_list_screen.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int _selectedIndex = 0;

  final List<Widget> _widgetOptions = <Widget>[
    AdsListScreen(AdType.Bikes),
    AdsListScreen(AdType.Parts),
    SizedBox(),
    EventsListScreen(),
    MoreMainScreen(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: UiUtils.appBar(context, Strings.generalAppName),
        body: Center(
          child: _widgetOptions.elementAt(_selectedIndex),
        ),
        bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          currentIndex: _selectedIndex,
          selectedItemColor: Theme.of(context).accentColor,
          onTap: (index) => _onItemTapped(context, index),
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(
                icon: Icon(Icons.directions_bike),
                title: Text(Strings.generalBikes)),
            BottomNavigationBarItem(
                icon: Icon(Icons.settings), title: Text(Strings.generalParts)),
            BottomNavigationBarItem(
                icon: Icon(Icons.add_circle_outline),
                title: Text(Strings.generalAdd)),
            BottomNavigationBarItem(
                icon: Icon(Icons.flag), title: Text(Strings.generalEvents)),
            BottomNavigationBarItem(
                icon: Icon(Icons.more_horiz), title: Text(Strings.generalMore))
          ],
        ));
  }

  void _onItemTapped(BuildContext context, int index) {
    if (index == 2)
      _addBikeAd(context);
    else
      setState(() => _selectedIndex = index);
  }

  void _addBikeAd(BuildContext context) async {
    final navigator = Navigator.of(context);
    final user = await FirebaseAuth.instance.currentUser();
    if (user == null)
      Navigator.of(context).pushNamed(LoginScreen.routeName);
    else if (_selectedIndex == 0 || _selectedIndex == 4) {
      navigator.pushNamed(AddNewAdScreen.routeName, arguments: AdType.Bikes);
    } else if (_selectedIndex == 1) {
      navigator.pushNamed(AddNewAdScreen.routeName, arguments: AdType.Parts);
    } else if (_selectedIndex == 3) {
      navigator.pushNamed(AddNewEventScreen.routeName);
    }
  }
}
