import 'package:bikenerd/ressources/strings.dart';
import 'package:bikenerd/utils/utils.dart';
import 'package:bikenerd/utils/validator.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class LoginScreen extends StatefulWidget {
  static final String routeName = '/login';

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _auth = FirebaseAuth.instance;
  final _formKey = GlobalKey<FormState>();
  var _isLogin = true;
  var _isLoading = false;
  String _userEmail = '';
  String _userName = '';
  String _userPassword = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: Center(
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8.0),
          ),
          elevation: 5,
          margin: const EdgeInsets.all(32),
          child: SingleChildScrollView(
            padding: const EdgeInsets.all(16),
            child: Form(
              key: _formKey,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  if (!_isLogin)
                    TextFormField(
                      key: ValueKey('username'),
                      textInputAction: TextInputAction.next,
                      decoration: InputDecoration(
                          labelText: Strings.loginUsername,
                          icon: Icon(Icons.account_circle)),
                      onSaved: (value) => _userName = value,
                      onFieldSubmitted: (_) =>
                          FocusScope.of(context).nextFocus(),
                      validator: _isUserNameValid,
                    ),
                  SizedBox(
                    height: 16,
                  ),
                  TextFormField(
                    key: ValueKey('email'),
                    textInputAction: TextInputAction.next,
                    keyboardType: TextInputType.emailAddress,
                    decoration: InputDecoration(
                      labelText: Strings.loginEmail,
                      icon: Icon(Icons.email),
                    ),
                    onSaved: (value) => _userEmail = value,
                    onFieldSubmitted: (_) => FocusScope.of(context).nextFocus(),
                    validator: Validator.isEmailValid,
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  TextFormField(
                    obscureText: true,
                    key: ValueKey('password'),
                    decoration: InputDecoration(
                        labelText: Strings.loginPassword,
                        icon: Icon(Icons.lock)),
                    onSaved: (value) => _userPassword = value,
                    validator: _isPasswordValid,
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Builder(
                    builder: (context) => _isLoading
                        ? CircularProgressIndicator()
                        : RaisedButton(
                            color: Theme.of(context).accentColor,
                            child: Text(
                              _isLogin
                                  ? Strings.loginLogin
                                  : Strings.loginSignUp,
                              style: TextStyle(color: Colors.white),
                            ),
                            onPressed: () => _submitAuthForm(context),
                          ),
                  ),
                  FlatButton(
                    textColor: Theme.of(context).primaryColor,
                    child: Text(
                      _isLogin
                          ? Strings.loginCreateAccount
                          : Strings.loginAlreadyHaveAccount,
                    ),
                    onPressed: _isLoading
                        ? null
                        : () {
                            setState(() {
                              _isLogin = !_isLogin;
                            });
                          },
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void _submitAuthForm(BuildContext context) async {
    final isValid = _formKey.currentState.validate();
    FocusScope.of(context).unfocus();

    if (isValid) {
      _formKey.currentState.save();
      _setLoadingState(true);

      try {
        AuthResult authResult;

        final isDeviceConnected = await Utils.isDeviceConnected();

        if (!isDeviceConnected) {
          Utils.showErrorSnackBar(context, Strings.generalErrorNoInternet);
          _setLoadingState(false);
          return;
        }

        if (_isLogin) {
          authResult = await _auth.signInWithEmailAndPassword(
              email: _userEmail, password: _userPassword);
        } else {
          authResult = await _auth.createUserWithEmailAndPassword(
              email: _userEmail, password: _userPassword);

          await Firestore.instance
              .collection('users')
              .document(authResult.user.uid)
              .setData({
            'username': _userName,
            'email': _userEmail,
            'createdAt': Timestamp.now(),
          });
        }
        Navigator.of(context).pushReplacementNamed('/');
      } on PlatformException catch (error) {
        final errorMessage = error.message != null
            ? error.message
            : Strings.loginCredentialsError;
        Utils.showErrorSnackBar(context, errorMessage);
        _setLoadingState(false);
      } catch (error) {
        Utils.showErrorSnackBar(context, Strings.loginGeneralError);
        _setLoadingState(false);
      }
    }
  }

  void _setLoadingState(bool value) {
    setState(() {
      _isLoading = value;
    });
  }

  String _isUserNameValid(String value) {
    return (value.isEmpty || value.trim().length < 4)
        ? Strings.loginIncorrectUsername
        : null;
  }

  String _isPasswordValid(String value) {
    return (value.isEmpty || value.trim().length < 7)
        ? Strings.loginIncorrectPassword
        : null;
  }
}
