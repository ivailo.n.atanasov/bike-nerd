import 'package:bikenerd/ads/screen/ad_details_screen.dart';
import 'package:bikenerd/app_theme.dart';
import 'package:bikenerd/ads/screen/add_new_ad_screen.dart';
import 'package:bikenerd/events/screen/add_new_event_screen.dart';
import 'package:bikenerd/home_screen.dart';
import 'package:bikenerd/login/screen/login_screen.dart';
import 'package:bikenerd/ressources/strings.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: Strings.generalAppName,
      theme: appTheme(),
      home: HomeScreen(),
      debugShowCheckedModeBanner: false,
      routes: {
        LoginScreen.routeName: (context) => LoginScreen(),
        AddNewAdScreen.routeName: (context) => AddNewAdScreen(),
        AdDetailsScreen.routeName: (context) => AdDetailsScreen(),
        AddNewEventScreen.routeName: (context) => AddNewEventScreen()
      },
    );
  }
}
