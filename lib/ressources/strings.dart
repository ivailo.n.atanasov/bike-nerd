class Strings {

  // General

  static const String generalAppName = "Bike Nerd";
  static const String generalBikes = "Bikes";
  static const String generalParts = "Parts";
  static const String generalEvents = "Events";
  static const String generalAccount = "Account";
  static const String generalMore = "More";
  static const String generalCamera = "Camera";
  static const String generalGallery = "Gallery";
  static const String generalDelete = "Delete";
  static const String generalYes = "Yes";
  static const String generalNo = "No";
  static const String generalConfirm = "Confirm";
  static const String generalCancel = "Cancel";
  static const String generalPublish = "Publish";
  static const String generalAdd = "Add";
  static const String generalWarning = "Warning";
  static String generalErrorNoInternet = "No internet connection found. Please try again later.";

  // Login screen

  static const String loginUsername = 'Username';
  static const String loginEmail = 'Email address';
  static const String loginPassword = 'Password';
  static const String loginLogin = 'Login';
  static const String loginSignUp = 'Sign Up';
  static const String loginCreateAccount = 'Create new account';
  static const String loginAlreadyHaveAccount = 'I already have an account';

  static const String loginIncorrectUsername = 'Please enter at least 4 characters';
  static const String loginIncorrectEmail = 'Please enter a valid email address';
  static const String loginIncorrectPassword = 'Password must be at least 7 characters long';

  static const String loginCredentialsError = "An error occurred, please check your credentials.";
  static const String loginGeneralError = "An error occurred, please try again.";

  // Ads

  static const String adsNoResultsFound = 'No results found';

  // Add ad screen

  static const String addCoverPicture = 'Cover Picture';
  static const String addExtraPicture = 'Extra Picture';
  static const String addTitle = 'Title *';
  static const String addDescription = 'Description *';
  static const String addPrice = 'Price *';
  static const String addCity = 'City *';
  static const String addPostalCode = 'Postal Code *';
  static const String addEmail = 'Contact Email *';
  static const String addPhone = 'Contact Phone';
  static const String addBikeSpecs = 'Optional Specs';
  static const String addBikeType = 'Bike Type *';
  static const String addSecondHandItem = "2nd Hand";
  static const String addBikeNerdsInfo = 'Extra optional values for bike nerds.';
  static const String adTitleBikes = 'Add New Bike Ad';
  static const String adTitleParts = 'Add New Bike Part Ad';
  static const String addNewBike = 'bike';
  static const String addNewPart = 'part';

  static const String addMissingPicture = 'Please add at least one picture';
  static const String addMissingBikeType = 'Please select bike type';
  static const String addIncorrectTitle = 'Please enter at least 8 characters';
  static const String addIncorrectDescription = 'Please enter at least 10 characters';
  static const String addIncorrectPrice = 'Please enter a valid price';
  static const String addIncorrectEMail = 'Please enter a valid email address';
  static const String addIncorrectPhone = 'Please enter a valid phone number';
  static const String addIncorrectWebsite = 'Please enter a valid website';
  static const String addIncorrectCityPostalCode = 'Please enter at least 4 characters';
  static const String addIncorrectCity = 'Please enter at least 3 characters';
  static const String addIncorrectValues = 'Incorrect values';
  static const String addProgressPublishing = 'Publishing your ad';

  // Add event screen

  static const String addEventName = 'Event name *';
  static const String addEventDate = 'Event date *';
  static const String addEventType = 'Event type *';
  static const String addEventDescription = 'Description *';
  static const String addEventPrice = 'Participation price';
  static const String addEventStartHours = 'Starting hours *';
  static const String addEventAddress = 'Exact Address *';
  static const String addEventCountry = 'Select country';
  static const String addEventProvence = 'Provence *';
  static const String addEventPhone = 'Phone number *';
  static const String addEventEmail = 'Contact e-mail *';
  static const String addEventWebsite = 'Event website';

  static const String addEventMissingDate = 'Please select a date for the event';
  static const String addEventMissingType = 'Please select an event type';
  static const String addEventIncorrectName = 'Please enter at least 5 characters';
  static const String addEventIncorrectDescription = 'Please enter at least 10 characters';
  static const String addEventIncorrectTime = 'Please enter at least 2 characters';
  static const String addEventIncorrectAddress = 'Please enter at least 8 characters';
  static const String addEventIncorrectPrice = 'Please enter at least 2 characters';
  static const String addEventIncorrectWebsite = 'Please enter at least 5 characters';

  // Event type

  static const String eventTypeRandonnee = 'Randonnee';
  static const String eventTypeXCountry = 'X-Country';
  static const String eventTypeEnduro = 'Enduro';
  static const String eventTypeNocturne = 'Nocturne';
  static const String eventEMtb = 'e-MTB';
  static const String eventTypeGravel = 'Gravel';
  static const String eventTypeMarathon = 'Marathon';
  static const String eventTypeMultiDisciplines = 'Multi Disciplines';

  // Bike types

  static const String bikeTypeCity = 'City Bike';
  static const String bikeTypeRoad = 'Road Bike';
  static const String bikeTypeElectric = 'Electric Bike';
  static const String bikeTypeMountain = 'Mountain Bike';
  static const String bikeTypeGravel = 'Gravel Bike';
  static const String bikeTypeKids = 'Kids Bike';

  // Bike Specs

  static const String bikeSpecs = 'Bike Specs';

  static const String bikeSpecsWebsite = 'Product website';

  static const String bikeSpecFrameset = 'Frameset';
  static const String bikeSpecWheels = 'Wheels';
  static const String bikeSpecDrivetrain = 'Drivetrain';
  static const String bikeSpecComponents = 'Components';

  static const String bikeSpecsFrame = 'Frame';
  static const String bikeSpecsFork = 'Fork';
  static const String bikeSpecsSuspension = 'Suspension';
  static const String bikeSpecsGeneral = 'General';

  static const String bikeSpecsFrontWheel = 'Front Wheel';
  static const String bikeSpecsRearWheel = 'Rear Wheel';
  static const String bikeSpecsTires = 'Tires';

  static const String bikeSpecksGears = 'Gears';
  static const String bikeSpecksSaddle = 'Saddle';
  static const String bikeSpecksHandleBar = 'Handlebar';
  static const String bikeSpecksStem = 'Stem';
  static const String bikeSpecksBreaks = 'Breaks';
  static const String bikeSpecksBattery = 'Battery';

  static const String bikeSpecksWeight = 'Weight';

  // Error Contact

  static const String contactErrorMail = 'No email app was found please try another method.';
  static const String contactErrorCall = 'No phone app was found please try another method.';
  static const String contactErrorSMS = 'No sms app was found please try another method.';
  static const String contactErrorBrowser = 'No browser application found';

  // User ads Screen

  static const String userAdsNoAds = 'You have no ads yet';
  static const String userAdsNoFavorite = 'You have no favorite ads yet';
  static const String userAdsDeleteAlert = 'Are you sure you want to delete this ad?';
  static const String userAdsChangePrice = 'Current price is ';
  static const String userAdsNewPrice = 'Enter new price ';
  static const String userAdsNewPriceSuccess = 'Price updated';

  // More screen

  static const String moreMyAds = 'My Ads';
  static const String moreFavorite = 'Favorite';
  static const String moreLogout = 'Logout';
  static const String moreAbout = 'About';
  static const String moreAboutDialog = 'This application is used for demo purpose only';

}