import 'package:bikenerd/events/model/event.dart';
import 'package:bikenerd/events/provider/event_provider.dart';
import 'package:bikenerd/ressources/strings.dart';
import 'package:bikenerd/utils/ui_utils.dart';
import 'package:bikenerd/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AddNewEventTypeWidget extends StatefulWidget {
  @override
  _AddNewEventTypeWidgetState createState() => _AddNewEventTypeWidgetState();
}

class _AddNewEventTypeWidgetState extends State<AddNewEventTypeWidget> {
  bool _isExpanded = false;
  String _selectedItem;

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<EventProvider>(context, listen: false);
    return Container(
      decoration: UiUtils.generalBoxDecoration(),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            child: GestureDetector(
              onTap: () {
                setState(() {
                  Utils.removeKeyboardFocus(context);
                  _isExpanded = !_isExpanded;
                });
              },
              child: Row(
                children: <Widget>[
                  SizedBox(
                    width: 8,
                  ),
                  Icon(
                    Icons.flag,
                    color: Theme.of(context).primaryColor,
                  ),
                  SizedBox(
                    width: 12,
                  ),
                  Expanded(
                      child: Text(
                    _selectedItem == null
                        ? Strings.addEventType
                        : _selectedItem,
                    style: TextStyle(
                        color:
                            _selectedItem == null ? Colors.grey : Colors.black),
                  )),
                  SizedBox(
                    width: 8,
                  ),
                  IconButton(
                    icon: _isExpanded
                        ? Icon(Icons.keyboard_arrow_up)
                        : Icon(Icons.keyboard_arrow_down),
                    onPressed: () {
                      setState(() {
                        _isExpanded = !_isExpanded;
                      });
                    },
                  ),
                ],
              ),
            ),
          ),
          if (_isExpanded)
            Container(
              color: Colors.grey.withAlpha(40),
              child: Column(
                children: <Widget>[
                  _getListTile(EventType.Randonnee, Strings.eventTypeRandonnee,
                      provider),
                  Divider(),
                  _getListTile(
                      EventType.XCountry, Strings.eventTypeXCountry, provider),
                  Divider(),
                  _getListTile(
                      EventType.Enduro, Strings.eventTypeEnduro, provider),
                  Divider(),
                  _getListTile(
                      EventType.Nocturne, Strings.eventTypeNocturne, provider),
                  Divider(),
                  _getListTile(EventType.eMTB, Strings.eventEMtb, provider),
                  Divider(),
                  _getListTile(
                      EventType.Gravel, Strings.eventTypeGravel, provider),
                  Divider(),
                  _getListTile(
                      EventType.Marathon, Strings.eventTypeMarathon, provider),
                  Divider(),
                  _getListTile(EventType.MultiDisciplines,
                      Strings.eventTypeMultiDisciplines, provider),
                ],
              ),
            )
        ],
      ),
    );
  }

  ListTile _getListTile(
      EventType eventType, String title, EventProvider provider) {
    return ListTile(
      title: Text(title),
      onTap: () => _onItemClicked(eventType, title, provider),
    );
  }

  void _onItemClicked(
      EventType eventType, String title, EventProvider provider) {
    setState(() {
      Utils.removeKeyboardFocus(context);
      provider.eventType = eventType;
      _isExpanded = false;
      _selectedItem = title;
    });
  }
}
