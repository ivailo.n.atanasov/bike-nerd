import 'package:bikenerd/events/provider/event_provider.dart';
import 'package:bikenerd/events/widget/add_new_event_date_time_widget.dart';
import 'package:bikenerd/events/widget/add_new_event_type_widget.dart';
import 'package:bikenerd/ressources/strings.dart';
import 'package:bikenerd/utils/ui_utils.dart';
import 'package:bikenerd/utils/utils.dart';
import 'package:bikenerd/utils/validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:provider/provider.dart';

class AddNewEventFormWidget extends StatefulWidget {
  @override
  _AddNewEventFormWidgetState createState() => _AddNewEventFormWidgetState();
}

class _AddNewEventFormWidgetState extends State<AddNewEventFormWidget> {
  final _formKey = GlobalKey<FormState>();

  String _name;
  String _description;
  String _startHours;
  String _address;
  String _email;
  String _phone;
  String _website;
  String _price;

  @override
  Widget build(BuildContext context) {
    final ProgressDialog progressDialog =
        UiUtils.progressDialog(context, Strings.addProgressPublishing);
    final provider = Provider.of<EventProvider>(context, listen: false);
    return Form(
      key: _formKey,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          SizedBox(
            height: 16,
          ),
          TextFormField(
            maxLength: 30,
            textCapitalization: TextCapitalization.sentences,
            textInputAction: TextInputAction.next,
            onFieldSubmitted: (_) => FocusScope.of(context).nextFocus(),
            decoration: InputDecoration(
              border: UiUtils.textFieldBorder(),
              labelText: Strings.addEventName,
            ),
            onSaved: (value) => _name = value,
            validator: (value) =>
                Validator.isTextValid(value, 5, Strings.addEventIncorrectName),
          ),
          _addSpace(),
          TextFormField(
            maxLength: 200,
            maxLines: 4,
            keyboardType: TextInputType.text,
            textInputAction: TextInputAction.done,
            textCapitalization: TextCapitalization.sentences,
            decoration: InputDecoration(
              border: UiUtils.textFieldBorder(),
              labelText: Strings.addEventDescription,
            ),
            onSaved: (value) => _description = value,
            validator: (value) => Validator.isTextValid(
                value, 10, Strings.addEventIncorrectDescription),
          ),
          SizedBox(
            height: 16,
          ),
          AddNewEventTypeWidget(),
          SizedBox(
            height: 16,
          ),
          AddNewEventDateTimeWidget(),
          SizedBox(
            height: 18,
          ),
          TextFormField(
            maxLength: 20,
            keyboardType: TextInputType.text,
            textInputAction: TextInputAction.next,
            onFieldSubmitted: (_) => FocusScope.of(context).nextFocus(),
            decoration: InputDecoration(
              border: UiUtils.textFieldBorder(),
              prefixIcon: Icon(
                Icons.timer,
                color: Theme.of(context).primaryColor,
              ),
              labelText: Strings.addEventStartHours,
            ),
            onSaved: (value) => _startHours = value,
            validator: (value) =>
                Validator.isTextValid(value, 2, Strings.addEventIncorrectTime),
          ),
          _addSpace(),
          TextFormField(
            maxLength: 30,
            keyboardType: TextInputType.text,
            textInputAction: TextInputAction.next,
            onFieldSubmitted: (_) => FocusScope.of(context).nextFocus(),
            decoration: InputDecoration(
              border: UiUtils.textFieldBorder(),
              prefixIcon: Icon(Icons.location_on,
                  color: Theme.of(context).primaryColor),
              labelText: Strings.addEventAddress,
            ),
            onSaved: (value) => _address = value,
            validator: (value) => Validator.isTextValid(
                value, 8, Strings.addEventIncorrectAddress),
          ),
          _addSpace(),
          TextFormField(
            maxLength: 20,
            keyboardType: TextInputType.number,
            textInputAction: TextInputAction.next,
            onFieldSubmitted: (_) => FocusScope.of(context).nextFocus(),
            decoration: InputDecoration(
              border: UiUtils.textFieldBorder(),
              prefixIcon: Icon(
                Icons.euro_symbol,
                color: Theme.of(context).primaryColor,
              ),
              labelText: Strings.addEventPrice,
            ),
            onSaved: (value) => _price = value,
            validator: (value) =>
                Validator.isTextValid(value, 2, Strings.addEventIncorrectPrice),
          ),
          _addSpace(),
          TextFormField(
            maxLength: 30,
            keyboardType: TextInputType.emailAddress,
            textInputAction: TextInputAction.next,
            onFieldSubmitted: (_) => FocusScope.of(context).nextFocus(),
            decoration: InputDecoration(
              border: UiUtils.textFieldBorder(),
              prefixIcon:
                  Icon(Icons.email, color: Theme.of(context).primaryColor),
              labelText: Strings.addEventEmail,
            ),
            onSaved: (value) => _email = value,
            validator: Validator.isEmailValid,
          ),
          _addSpace(),
          TextFormField(
            maxLength: 20,
            keyboardType: TextInputType.number,
            decoration: InputDecoration(
              border: UiUtils.textFieldBorder(),
              prefixIcon:
                  Icon(Icons.phone, color: Theme.of(context).primaryColor),
              labelText: Strings.addEventPhone,
            ),
            onSaved: (value) => _phone = value,
            validator: (value) => Validator.isPhoneValid(value, true),
          ),
          _addSpace(),
          TextFormField(
            maxLength: 50,
            keyboardType: TextInputType.url,
            decoration: InputDecoration(
              border: UiUtils.textFieldBorder(),
              prefixIcon:
                  Icon(Icons.language, color: Theme.of(context).primaryColor),
              labelText: Strings.addEventWebsite,
            ),
            onSaved: (value) => _website = value,
            validator: (value) => Validator.isWebsiteValid(value, false),
          ),
          SizedBox(height: 40),
          Container(
            width: double.infinity,
            child: Builder(
              builder: (context) => RaisedButton(
                padding: EdgeInsets.symmetric(vertical: 16),
                child: Text(
                  Strings.generalPublish,
                  style: TextStyle(color: Colors.white, fontSize: 16),
                ),
                color: Theme.of(context).accentColor,
                onPressed: () =>
                    _validateAndPublish(provider, context, progressDialog),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _addSpace() {
    return SizedBox(height: 12);
  }

  Future<void> _validateAndPublish(EventProvider provider, BuildContext context,
      ProgressDialog progressDialog) async {
    if (!_isEventValid(provider)) return;

    final isConnected = await Utils.isDeviceConnected();

    if (!isConnected) {
      Utils.showErrorSnackBar(context, Strings.generalErrorNoInternet);
      return;
    }

    try {
      await progressDialog.show();
      _formKey.currentState.save();
      await provider.publishEvent(
          name: _name,
          description: _description,
          startHours: _startHours,
          address: _address,
          email: _email,
          phone: _phone,
          website: _website,
          price: _price);

      await progressDialog.hide();
      Navigator.of(context).pop();
    } on PlatformException catch (error) {
      print(error);
      final errorMessage =
          error.message != null ? error.message : Strings.loginGeneralError;
      await progressDialog.hide();
      Utils.showErrorSnackBar(context, errorMessage);
    } catch (error) {
      print(error);
      await progressDialog.hide();
      Utils.showErrorSnackBar(context, Strings.loginGeneralError);
    }
  }

  bool _isEventValid(EventProvider provider) {
    Utils.removeKeyboardFocus(context);

    if (provider.eventType == null) {
      Utils.showErrorSnackBar(context, Strings.addEventMissingType);
      return false;
    }

    if (provider.eventDate == null) {
      Utils.showErrorSnackBar(context, Strings.addEventMissingDate);
      return false;
    }

    final isValid = _formKey.currentState.validate();

    if (!isValid) {
      Utils.showErrorSnackBar(context, Strings.addIncorrectValues);
    }

    return isValid;
  }
}
