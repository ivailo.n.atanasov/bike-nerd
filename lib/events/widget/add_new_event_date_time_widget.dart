import 'package:bikenerd/events/provider/event_provider.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class AddNewEventDateTimeWidget extends StatefulWidget {
  @override
  _AddNewEventDateTimeWidgetState createState() =>
      _AddNewEventDateTimeWidgetState();
}

class _AddNewEventDateTimeWidgetState extends State<AddNewEventDateTimeWidget> {
  DateTime _date;

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<EventProvider>(context, listen: false);
    return Container(
        height: 50,
        width: double.infinity,
        child: OutlineButton.icon(
          icon: Icon(
            Icons.date_range,
            color: Theme.of(context).primaryColor,
          ),
          shape: RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(5.0)),
          textColor: Theme.of(context).primaryColor,
          borderSide: BorderSide(color: Theme.of(context).primaryColor),
          focusColor: Theme.of(context).primaryColor,
          color: Theme.of(context).primaryColor,
          label: Text(_date == null ? 'Select Date' : _formatDate(_date)),
          onPressed: () => _selectDate(context, provider),
        ));
  }

  _selectDate(BuildContext context, EventProvider provider) async {
    final currentYear = DateTime.now().year;

    final selectedDate = await showDatePicker(
        context: context,
        initialDate: _date == null ? DateTime.now() : _date,
        firstDate: DateTime(currentYear),
        lastDate: DateTime(currentYear + 1));

    if (selectedDate != null) {
      setState(() {
        provider.eventDate = selectedDate;
        _date = selectedDate;
      });
    }
  }

  String _formatDate(DateTime dateTime) {
    return DateFormat.yMMMd().format(dateTime);
  }
}
