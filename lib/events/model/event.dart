import 'package:flutter/material.dart';

class Event {
  String creatorId;
  String name;
  String description;
  DateTime date;
  EventType eventType;
  String address;
  String startingHours;
  String price;
  String phoneNumber;
  String email;
  String website;

  Event({
    @required this.creatorId,
    @required this.name,
    @required this.description,
    @required this.date,
    @required this.eventType,
    @required this.address,
    @required this.startingHours,
    @required this.price,
    @required this.phoneNumber,
    @required this.email,
    @required this.website,
  });
}

enum EventType {
  Randonnee,
  XCountry,
  Enduro,
  Nocturne,
  eMTB,
  Gravel,
  Marathon,
  MultiDisciplines
}
