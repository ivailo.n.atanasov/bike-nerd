import 'package:bikenerd/ressources/strings.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class EventsListScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Padding(
      padding: const EdgeInsets.all(6),
      child: StreamBuilder<QuerySnapshot>(
        stream: Firestore.instance.collection('events').snapshots(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasError) return new Text('Error: ${snapshot.error}');
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(child: CircularProgressIndicator());
          } else {
            if (snapshot.hasData && snapshot.data.documents.length > 0) {
              return _buildEventsList(context, snapshot);
            } else {
              return Center(
                  child: Text(
                Strings.adsNoResultsFound,
                style: TextStyle(color: Colors.grey),
              ));
            }
          }
        },
      ),
    ));
  }

  Widget _buildEventsList(
      BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
    return ListView.builder(
        itemCount: snapshot.data.documents.length,
        itemBuilder: (ctx, index) {
          final document = snapshot.data.documents[index].data;
          return Card(
            elevation: 4,
            child: Container(
              padding: const EdgeInsets.all(8),
              child: ListTile(
                leading: CircleAvatar(
                  backgroundImage: AssetImage('assets/images/race.png'),
                ),
                title: Text(
                  document['event_name'],
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                ),
                subtitle: Text(document['description']),
                contentPadding: const EdgeInsets.all(4),
              ),
            ),
          );
        });
  }
}
