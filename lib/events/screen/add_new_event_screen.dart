import 'package:bikenerd/events/provider/event_provider.dart';
import 'package:bikenerd/events/widget/add_new_event_form_widget.dart';
import 'package:bikenerd/utils/ui_utils.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AddNewEventScreen extends StatelessWidget {
  static final String routeName = '/add_event';

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => EventProvider(),
      child: Scaffold(
        appBar: UiUtils.appBar(context, 'Add new event'),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(16),
            child: Center(
              child: Column(children: <Widget>[
                AddNewEventFormWidget(),
              ]),
            ),
          ),
        ),
      ),
    );
  }
}
