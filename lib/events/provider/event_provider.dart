import 'package:bikenerd/events/model/event.dart';
import 'package:bikenerd/utils/firebase_api.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class EventProvider with ChangeNotifier {
  DateTime eventDate;
  EventType eventType;

  Future<void> publishEvent(
      {@required String name,
      @required String description,
      @required String startHours,
      @required address,
      @required email,
      @required phone,
      @required String website,
      @required String price}) async {
    final user = await FirebaseAuth.instance.currentUser();

    final result = await FirebaseApi.uploadEvent(Event(
        creatorId: user.uid,
        name: name,
        description: description,
        date: eventDate,
        eventType: eventType,
        address: address,
        startingHours: startHours,
        price: price,
        phoneNumber: phone,
        email: email,
        website: website.isEmpty ? null : website));

    FirebaseApi.addToUserEvents(result.documentID);
  }
}
