import 'package:bikenerd/ressources/strings.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class Utils {
  static void showErrorSnackBar(BuildContext context, String message) {
    Scaffold.of(context).showSnackBar(SnackBar(
      content: Text(message),
      backgroundColor: Theme.of(context).errorColor,
    ));
  }

  static void showSuccessSnackBar(BuildContext context, String message) {
    Scaffold.of(context).showSnackBar(SnackBar(
      content: Text(message),
      backgroundColor: Theme.of(context).primaryColor,
    ));
  }

  static Future<bool> isDeviceConnected() async {
    var result = await Connectivity().checkConnectivity();
    return result != ConnectivityResult.none;
  }

  static removeKeyboardFocus(BuildContext context) {
    FocusScopeNode currentFocus = FocusScope.of(context);

    if (!currentFocus.hasPrimaryFocus) {
      currentFocus.unfocus();
    }
  }

  static Future<void> sendUrl(
      BuildContext context, String url, String errorMessage) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      Utils.showErrorSnackBar(context, errorMessage);
    }
  }

  static String getDisplayedPrice(double price) {
    return '€${price.truncate()}';
  }

  static String getDisplayedBike(int bikeType) {
    switch (bikeType) {
      case 0:
        return Strings.bikeTypeMountain;
      case 1:
        return Strings.bikeTypeRoad;
      case 2:
        return Strings.bikeTypeGravel;
      case 3:
        return Strings.bikeTypeCity;
      case 4:
        return Strings.bikeTypeElectric;
      case 5:
        return Strings.bikeTypeCity;
      default:
        return '';
    }
  }
}
