import 'package:flutter/material.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';
import 'package:progress_dialog/progress_dialog.dart';

class UiUtils {
  static BoxDecoration generalBoxDecoration() {
    return BoxDecoration(
        border: Border.all(width: 0.5, color: Colors.grey),
        borderRadius: BorderRadius.all(Radius.circular(5)));
  }

  static OutlineInputBorder textFieldBorder() {
    return OutlineInputBorder(
      borderRadius: BorderRadius.circular(5.0),
      borderSide: BorderSide(
        color: Colors.grey,
        width: 2.0,
      ),
    );
  }

  static ProgressDialog progressDialog(BuildContext context, String msg) {
    final progressColor =
        new AlwaysStoppedAnimation<Color>(Theme.of(context).accentColor);
    ProgressDialog progressDialog = ProgressDialog(context);
    progressDialog.style(
        padding: const EdgeInsets.all(16),
        progressWidget: CircularProgressIndicator(
          valueColor: progressColor,
        ),
        message: msg);
    return progressDialog;
  }

  static GradientAppBar appBar(BuildContext context, String title) {
    return GradientAppBar(
      gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [Theme.of(context).primaryColor, Colors.white24]),
      title: Text(title),
    );
  }
}
