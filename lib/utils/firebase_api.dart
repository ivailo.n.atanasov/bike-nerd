import 'dart:io';

import 'package:bikenerd/ads/model/ad.dart';
import 'package:bikenerd/events/model/event.dart';
import 'package:bikenerd/more/model/user_ad_type.dart';
import 'package:bikenerd/ressources/strings.dart';
import 'package:bikenerd/utils/utils.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:uuid/uuid.dart';

class FirebaseApi {
  static Future<DocumentReference> uploadAd(AdType adType, Ad ad) async {
    final result =
        await Firestore.instance.collection(getCollectionName(adType)).add({
      'creator_uid': ad.creatorId,
      'ad_pictures': ad.adPictures,
      'title': ad.title,
      'description': ad.description,
      'ad_type': ad.adType,
      'price': ad.price,
      'city': ad.city,
      'postal_code': ad.postalCode,
      'bike_type': ad.bikeType,
      'bike_specs': ad.bikeSpecs,
      'contact_email': ad.contactEmail,
      'contact_phone': ad.contactPhone,
      'created_at': ad.createdAt,
    });
    return result;
  }

  static Future<DocumentReference> uploadEvent(Event event) async {
    final result = await Firestore.instance.collection('events').add({
      'creator_id': event.creatorId,
      'event_name': event.name,
      'description': event.description,
      'date': event.date.toIso8601String(),
      'event_type': event.eventType.index,
      'address': event.address,
      'starting_hours': event.startingHours,
      'price': event.price,
      'phone_number': event.phoneNumber,
      'email': event.email,
      'website': event.website,
    });
    return result;
  }

  static Future<AdPicture> uploadAdPicture(AdType adType, File file) async {
    final String uuid = Uuid().v1();
    final ref = FirebaseStorage.instance
        .ref()
        .child(getCollectionName(adType))
        .child('$uuid.jpg');

    await ref.putFile(file).onComplete;

    final url = await ref.getDownloadURL();
    return AdPicture(uuid, url);
  }

  static Future<void> addToUserAds(String adId, int adType) async {
    final user = await FirebaseAuth.instance.currentUser();
    await Firestore.instance
        .collection('users')
        .document(user.uid)
        .collection('ads')
        .add({'ad_id': adId, 'ad_type': adType});
  }

  static Future<void> addToUserEvents(String adId) async {
    final user = await FirebaseAuth.instance.currentUser();
    await Firestore.instance
        .collection('users')
        .document(user.uid)
        .collection('events')
        .add({'ad_id': adId});
  }

  static Future<void> setAdFavorite(
      bool isFavorite, String adId, int adType) async {
    final user = await FirebaseAuth.instance.currentUser();
    final collection = Firestore.instance
        .collection('users')
        .document(user.uid)
        .collection('favorite');

    if (isFavorite) {
      await collection.add({'ad_id': adId, 'ad_type': adType});
    } else {
      final result =
          await collection.where('ad_id', isEqualTo: adId).getDocuments();
      result.documents.forEach((document) {
        document.reference.delete();
      });
    }
  }

  static Future<List<String>> getFavoriteAdsId() async {
    final List<String> favorite = [];
    final user = await FirebaseAuth.instance.currentUser();
    if (user == null) return favorite;
    final collection = await Firestore.instance
        .collection('users')
        .document(user.uid)
        .collection('favorite')
        .getDocuments();
    collection.documents.forEach((document) {
      favorite.add(document['ad_id']);
    });
    return favorite;
  }

  static Future<List<DocumentSnapshot>> getUserAds(UserAdType adType) async {
    final List<DocumentSnapshot> favoriteAds = [];
    final user = await FirebaseAuth.instance.currentUser();
    if (user == null) return favoriteAds;
    final collection = await Firestore.instance
        .collection('users')
        .document(user.uid)
        .collection(adType.getName())
        .getDocuments();

    if (collection.documents.length == 0) return favoriteAds;

    for (final doc in collection.documents) {
      final ad = await Firestore.instance
          .collection(getCollectionFromInt(doc['ad_type']))
          .document(doc['ad_id'])
          .get();

      if (ad.data != null) {
        favoriteAds.add(ad);
      } else if (adType.userAd == UserAd.Favorite) {
        // ad was deleted so remove it's reference from favorites list
        doc.reference.delete();
      }
    }
    return favoriteAds;
  }

  static Future<void> removeUserAd(
      BuildContext context, DocumentSnapshot removedDocument) async {
    final isConnected = await Utils.isDeviceConnected();
    if (!isConnected) {
      Utils.showErrorSnackBar(context, Strings.generalErrorNoInternet);
      return;
    }

    final user = await FirebaseAuth.instance.currentUser();

    // Remove from user ads
    final result = await Firestore.instance
        .collection('users')
        .document(user.uid)
        .collection('ads')
        .where('ad_id', isEqualTo: removedDocument.documentID)
        .getDocuments();

    result.documents.forEach((element) async {
      await element.reference.delete();
    });

    // Delete cover picture
    final storage = FirebaseStorage.instance;
    final coverPicture = await storage.getReferenceFromUrl(
        removedDocument['ad_pictures']['picture_1']['url']);
    await coverPicture.delete();

    // Remove from ads
    await removedDocument.reference.delete();
  }

  static Future<void> updateDocumentValue(
      String key, dynamic newValue, DocumentSnapshot documentSnapshot) async {
    final reference = documentSnapshot.reference;
    await Firestore.instance.runTransaction((transaction) async {
      final DocumentSnapshot postSnapshot = await transaction.get(reference);
      if (postSnapshot.exists) {
        await transaction.update(reference, {key: newValue});
      }
    });
  }

  static String getCollectionName(AdType adType) {
    switch (adType) {
      case AdType.Bikes:
        return 'bikes';
      case AdType.Parts:
        return 'parts';
      default:
        return 'bikes';
    }
  }

  static String getCollectionFromInt(int adType) {
    switch (adType) {
      case 0:
        return 'bikes';
      case 1:
        return 'parts';
      default:
        return 'bikes';
    }
  }

  static AdType getAdTypeFromInt(int type) {
    switch (type) {
      case 0:
        return AdType.Bikes;
      case 1:
        return AdType.Parts;
      default:
        return AdType.Bikes;
    }
  }
}
