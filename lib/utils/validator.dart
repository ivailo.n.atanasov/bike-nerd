import 'package:bikenerd/ressources/strings.dart';

class Validator {
  static String isTextValid(String text, int length, String errorMsg) {
    if (text.trim().isEmpty || text.length < length) {
      return errorMsg;
    } else
      return null;
  }

  static String isPhoneValid(String phoneNumber, bool isMandatory) {
    if (phoneNumber.isEmpty) if (isMandatory)
      return Strings.addIncorrectPhone;
    else
      return null;
    else
      return phoneNumber.trim().length > 6 ? null : Strings.addIncorrectPhone;
  }

  static String isEmailValid(String value) {
    if (value.isEmpty) return Strings.loginIncorrectEmail;
    if (value.trim().length < 5) return Strings.loginIncorrectEmail;
    if (!value.trim().contains('@') || !value.trim().contains('.'))
      return Strings.loginIncorrectEmail;
    return null;
  }

  static String isPriceValid(String price) {
    if (price.trim().isEmpty) {
      return Strings.addIncorrectPrice;
    } else
      try {
        double.parse(price);
        return null;
      } catch (error) {
        return Strings.addIncorrectPrice;
      }
  }

  static String isWebsiteValid(String website, bool isMandatory) {
    if (website.isEmpty) {
      if (isMandatory) {
        return Strings.addIncorrectPhone;
      } else {
        return null;
      }
    }

    if (!website.contains('.')) {
      return Strings.addIncorrectWebsite;
    }

    return website.trim().length > 8 ? null : Strings.addIncorrectWebsite;
  }
}
