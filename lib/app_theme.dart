import 'package:flutter/material.dart';

ThemeData appTheme() {
  return ThemeData(
      brightness: Brightness.light,
      primarySwatch: Colors.teal,
      accentColor: Colors.amber,
      fontFamily: 'Roboto',
      textTheme: TextTheme(
          headline1: TextStyle(fontSize: 72, fontWeight: FontWeight.bold),
          headline6: TextStyle(fontSize: 36, fontWeight: FontWeight.bold),
          bodyText2: TextStyle(fontSize: 18)));
}
