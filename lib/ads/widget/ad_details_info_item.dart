import 'package:flutter/material.dart';

class AdDetailsInfoItemWidget extends StatelessWidget {
  final String title;
  final IconData iconData;

  AdDetailsInfoItemWidget(this.title, this.iconData);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
      width: double.infinity,
      child: Row(
        children: <Widget>[
          Icon(
            iconData,
            color: Theme.of(context).primaryColor,
          ),
          SizedBox(
            width: 16,
          ),
          Expanded(
              child: Text(
            title,
            textAlign: TextAlign.start,
            style: TextStyle(color: Colors.grey),
          ))
        ],
      ),
    );
  }
}
