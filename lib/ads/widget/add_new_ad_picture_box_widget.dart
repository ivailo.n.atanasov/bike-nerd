import 'dart:io';

import 'package:bikenerd/ressources/strings.dart';
import 'package:bikenerd/utils/ui_utils.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class AddNewAdPictureBoxWidget extends StatefulWidget {
  final double width;
  final double height;
  final bool isMainPicture;
  final Function(File file, bool isDelete) function;

  AddNewAdPictureBoxWidget(
      {@required this.width,
      @required this.height,
      @required this.isMainPicture,
      @required this.function});

  @override
  _AddNewAdPictureBoxWidgetState createState() =>
      _AddNewAdPictureBoxWidgetState();
}

class _AddNewAdPictureBoxWidgetState extends State<AddNewAdPictureBoxWidget> {
  File _imageFile;
  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => _displayPictureChoice(context),
      splashColor: Theme.of(context).primaryColor,
//      radius: 20,
      child: Container(
        width: widget.width,
        height: widget.height,
        decoration: UiUtils.generalBoxDecoration(),
        child: _imageFile != null
            ? Image.file(
                _imageFile,
                fit: BoxFit.cover,
              )
            : Center(
                child: _isLoading
                    ? CircularProgressIndicator()
                    : Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Icon(
                            Icons.add,
                            color: Theme.of(context).primaryColor,
                          )
                        ],
                      )),
      ),
    );
  }

  void _displayPictureChoice(BuildContext context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Container(
            child: new Wrap(
              children: <Widget>[
                new ListTile(
                    leading: new Icon(
                      Icons.camera_alt,
                      color: Theme.of(context).primaryColor,
                    ),
                    title: new Text(Strings.generalCamera),
                    onTap: () {
                      _getImage(ImageSource.camera);
                    }),
                new ListTile(
                  leading: new Icon(Icons.photo_library,
                      color: Theme.of(context).primaryColor),
                  title: new Text(Strings.generalGallery),
                  onTap: () {
                    _getImage(ImageSource.gallery);
                  },
                ),
                if (_imageFile != null && !widget.isMainPicture)
                  new ListTile(
                    leading: new Icon(Icons.delete,
                        color: Theme.of(context).primaryColor),
                    title: new Text(Strings.generalDelete),
                    onTap: () {
                      Navigator.of(context).pop();
                      setState(() {
                        widget.function(_imageFile, true);
                        _isLoading = false;
                        _imageFile = null;
                      });
                    },
                  ),
              ],
            ),
          );
        });
  }

  Future<void> _getImage(ImageSource imageSource) async {
    setState(() {
      _isLoading = true;
    });
    Navigator.of(context).pop();

    final pickedFile =
        await ImagePicker.pickImage(source: imageSource, imageQuality: 15);

    setState(() {
      if (pickedFile == null) {
        _isLoading = false;
      } else {
        _imageFile = pickedFile;
        widget.function(pickedFile, false);
      }
    });
  }
}
