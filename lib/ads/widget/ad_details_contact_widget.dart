import 'package:bikenerd/ressources/strings.dart';
import 'package:bikenerd/utils/utils.dart';
import 'package:flutter/material.dart';

class AdDetailsContactWidget extends StatelessWidget {
  final String email;
  final String title;
  final String phone;

  AdDetailsContactWidget(this.email, this.title, this.phone);

  @override
  Widget build(BuildContext context) {
    final primaryColor = Theme.of(context).primaryColor;
    return Container(
      width: double.infinity,
      child: Row(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8),
            child: OutlineButton.icon(
                onPressed: () {
                  Utils.sendUrl(context, 'mailto:$email?subject=$title',
                      Strings.contactErrorMail);
                },
                icon: Icon(
                  Icons.mail,
                  color: primaryColor,
                ),
                label: Text('E-MAIL')),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8),
            child: OutlineButton.icon(
                onPressed: () {
                  Utils.sendUrl(
                      context, 'tel:$phone', Strings.contactErrorCall);
                },
                icon: Icon(
                  Icons.phone_android,
                  color: primaryColor,
                ),
                label: Text('CALL')),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8),
            child: OutlineButton.icon(
                onPressed: () {
                  Utils.sendUrl(context, 'sms:$phone', Strings.contactErrorSMS);
                },
                icon: Icon(
                  Icons.sms,
                  color: primaryColor,
                ),
                label: Text('SMS')),
          ),
        ],
      ),
    );
  }
}
