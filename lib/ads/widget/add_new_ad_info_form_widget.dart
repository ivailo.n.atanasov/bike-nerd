import 'package:bikenerd/ads/model/ad.dart';
import 'package:bikenerd/ads/provider/ad_provider.dart';
import 'package:bikenerd/ads/widget/add_new_ad_bike_specs_widget.dart';
import 'package:bikenerd/ads/widget/add_new_ad_bike_type_widget.dart';
import 'package:bikenerd/ressources/strings.dart';
import 'package:bikenerd/utils/ui_utils.dart';
import 'package:bikenerd/utils/utils.dart';
import 'package:bikenerd/utils/validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:provider/provider.dart';

import 'add_new_ad_pictures_selection_widget.dart';

class AdNewAdInfoFormWidget extends StatefulWidget {
  final AdType adType;

  AdNewAdInfoFormWidget(this.adType);

  @override
  _AdNewAdInfoFormWidgetState createState() => _AdNewAdInfoFormWidgetState();
}

class _AdNewAdInfoFormWidgetState extends State<AdNewAdInfoFormWidget> {
  final _formKey = GlobalKey<FormState>();
  String _title;
  String _description;
  String _city;
  String _postalCode;
  String _email;
  String _phone;
  double _price;

  @override
  Widget build(BuildContext context) {
    final ProgressDialog pr =
        UiUtils.progressDialog(context, Strings.addProgressPublishing);

    final primaryColor = Theme.of(context).primaryColor;

    return Form(
      key: _formKey,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          TextFormField(
            maxLength: 30,
            textCapitalization: TextCapitalization.sentences,
            textInputAction: TextInputAction.next,
            onFieldSubmitted: (_) => FocusScope.of(context).nextFocus(),
            decoration: InputDecoration(
              border: UiUtils.textFieldBorder(),
              labelText: Strings.addTitle,
            ),
            onSaved: (value) => _title = value,
            validator: (value) =>
                Validator.isTextValid(value, 8, Strings.addIncorrectTitle),
          ),
          SizedBox(height: 16),
          TextFormField(
            maxLength: 200,
            maxLines: 3,
            keyboardType: TextInputType.text,
            textInputAction: TextInputAction.done,
            textCapitalization: TextCapitalization.sentences,
            decoration: InputDecoration(
              border: UiUtils.textFieldBorder(),
              labelText: Strings.addDescription,
            ),
            onSaved: (value) => _description = value,
            validator: (value) => Validator.isTextValid(
                value, 10, Strings.addIncorrectDescription),
          ),
          SizedBox(height: 16),
          AddNewAdPicturesSelectionWidget(),
          SizedBox(height: 16),
          SizedBox(
            height: 12,
          ),
          if (widget.adType != AdType.Parts) AddNewAdBikeTypeWidget(),
          SizedBox(
            height: 12,
          ),
          SizedBox(
            height: 12,
          ),
          if (widget.adType != AdType.Parts) AddNewAdBikeSpecsWidget(),
          SizedBox(
            height: 16,
          ),
          //TODO this should be exported to a widget to avoid code repetition
          TextFormField(
            maxLength: 6,
            keyboardType: TextInputType.number,
            textInputAction: TextInputAction.next,
            onFieldSubmitted: (_) => FocusScope.of(context).nextFocus(),
            decoration: InputDecoration(
              border: UiUtils.textFieldBorder(),
              prefixIcon: Icon(
                Icons.euro_symbol,
                color: primaryColor,
              ),
              labelText: Strings.addPrice,
            ),
            onSaved: (value) => _price = double.parse(value),
            validator: Validator.isPriceValid,
          ),
          SizedBox(height: 16),
          TextFormField(
            maxLength: 20,
            textCapitalization: TextCapitalization.sentences,
            textInputAction: TextInputAction.next,
            onFieldSubmitted: (_) => FocusScope.of(context).nextFocus(),
            decoration: InputDecoration(
              border: UiUtils.textFieldBorder(),
              prefixIcon: Icon(
                Icons.location_city,
                color: primaryColor,
              ),
              labelText: Strings.addCity,
            ),
            onSaved: (value) => _city = value,
            validator: (value) =>
                Validator.isTextValid(value, 3, Strings.addIncorrectCity),
          ),
          SizedBox(height: 16),
          TextFormField(
            maxLength: 6,
            keyboardType: TextInputType.number,
            textInputAction: TextInputAction.next,
            onFieldSubmitted: (_) => FocusScope.of(context).nextFocus(),
            decoration: InputDecoration(
              border: UiUtils.textFieldBorder(),
              prefixIcon: Icon(
                Icons.markunread_mailbox,
                color: primaryColor,
              ),
              labelText: Strings.addPostalCode,
            ),
            onSaved: (value) => _postalCode = value,
            validator: (value) => Validator.isTextValid(
                value, 4, Strings.addIncorrectCityPostalCode),
          ),
          SizedBox(height: 16),
          TextFormField(
            maxLength: 30,
            keyboardType: TextInputType.emailAddress,
            textInputAction: TextInputAction.next,
            onFieldSubmitted: (_) => FocusScope.of(context).nextFocus(),
            decoration: InputDecoration(
              border: UiUtils.textFieldBorder(),
              prefixIcon: Icon(Icons.email, color: primaryColor),
              labelText: Strings.addEmail,
            ),
            onSaved: (value) => _email = value,
            validator: Validator.isEmailValid,
          ),
          SizedBox(height: 16),
          TextFormField(
              maxLength: 20,
              keyboardType: TextInputType.number,
              decoration: InputDecoration(
                border: UiUtils.textFieldBorder(),
                prefixIcon: Icon(Icons.phone, color: primaryColor),
                labelText: Strings.addPhone,
              ),
              onSaved: (value) => _phone = value,
              validator: (value) => Validator.isPhoneValid(value, false)),
          SizedBox(height: 32),
          Container(
            width: double.infinity,
            child: Builder(
              builder: (context) => RaisedButton(
                padding: EdgeInsets.symmetric(vertical: 16),
                child: Text(
                  Strings.generalPublish,
                  style: TextStyle(color: Colors.white, fontSize: 16),
                ),
                color: Theme.of(context).accentColor,
                onPressed: () => _validateAndPublish(context, pr),
              ),
            ),
          )
        ],
      ),
    );
  }

  void _validateAndPublish(
      BuildContext context, ProgressDialog progressDialog) async {
    final adProvider = Provider.of<AdProvider>(context, listen: false);

    if (!_isAdValid(adProvider)) return;

    final isConnected = await Utils.isDeviceConnected();

    if (!isConnected) {
      Utils.showErrorSnackBar(context, Strings.generalErrorNoInternet);
      return;
    }
    _publishAd(context, adProvider, progressDialog);
  }

  Future<void> _publishAd(BuildContext context, AdProvider adProvider,
      ProgressDialog progressDialog) async {
    try {
      await progressDialog.show();
      _formKey.currentState.save();
      await adProvider.publishAd(
          adType: widget.adType,
          title: _title,
          city: _city,
          postcode: _postalCode,
          description: _description,
          price: _price,
          email: _email,
          phone: _phone);

      await progressDialog.hide();
      Navigator.of(context).pop();
    } on PlatformException catch (error) {
      print(error);
      final errorMessage =
          error.message != null ? error.message : Strings.loginGeneralError;
      await progressDialog.hide();
      Utils.showErrorSnackBar(context, errorMessage);
    } catch (error) {
      print(error);
      await progressDialog.hide();
      Utils.showErrorSnackBar(context, Strings.loginGeneralError);
    }
  }

  bool _isAdValid(AdProvider adProvider) {
    Utils.removeKeyboardFocus(context);

    if (adProvider.adPictureFiles[0] == null) {
      Utils.showErrorSnackBar(context, Strings.addMissingPicture);
      return false;
    }

    if (widget.adType != AdType.Parts && adProvider.bikeType == null) {
      Utils.showErrorSnackBar(context, Strings.addMissingBikeType);
      return false;
    }

    final isValid = _formKey.currentState.validate();

    if (!isValid) {
      Utils.showErrorSnackBar(context, Strings.addIncorrectValues);
    }

    return isValid;
  }
}
