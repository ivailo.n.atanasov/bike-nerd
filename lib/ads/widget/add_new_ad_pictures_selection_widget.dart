import 'package:bikenerd/ads/provider/ad_provider.dart';
import 'package:bikenerd/ads/widget/add_new_ad_picture_box_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AddNewAdPicturesSelectionWidget extends StatefulWidget {
  @override
  _AddNewAdPicturesSelectionWidgetState createState() =>
      _AddNewAdPicturesSelectionWidgetState();
}

class _AddNewAdPicturesSelectionWidgetState
    extends State<AddNewAdPicturesSelectionWidget> {
  @override
  Widget build(BuildContext context) {
    final adProvider = Provider.of<AdProvider>(context, listen: false);
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            AddNewAdPictureBoxWidget(
                width: 100,
                height: 100,
                isMainPicture: true,
                function: (file, bool isDelete) {
                  adProvider.adPictureFiles[0] = isDelete ? null : file;
                  setState(() {});
                }),
            if (adProvider.adPictureFiles[0] != null)
              AddNewAdPictureBoxWidget(
                  width: 100,
                  height: 100,
                  isMainPicture: false,
                  function: (file, bool isDelete) {
                    adProvider..adPictureFiles[1] = isDelete ? null : file;
                    setState(() {});
                  }),
            if (adProvider.adPictureFiles[0] != null)
              AddNewAdPictureBoxWidget(
                  width: 100,
                  height: 100,
                  isMainPicture: false,
                  function: (file, bool isDelete) {
                    adProvider..adPictureFiles[2] = isDelete ? null : file;
                    setState(() {});
                  }),
          ],
        ),
      ],
    );
  }
}
