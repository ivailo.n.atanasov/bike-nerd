import 'package:bikenerd/ads/widget/ad_details_picture_zoom_in.dart';
import 'package:flutter/material.dart';
import 'package:carousel_pro/carousel_pro.dart';

class AdDetailsImagesWidget extends StatelessWidget {
  final List<String> urls;
  final String heroTag;

  AdDetailsImagesWidget(this.urls, this.heroTag);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 440,
      child: Hero(
        tag: heroTag,
        child: Carousel(
          autoplay: false,
          dotSize: 4,
          onImageTap: (index) {
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => AdDetailsPictureZoom(urls[index])),
            );
          },
          images: urls
              .map((e) => Container(
                    width: double.infinity,
                    child: Image(
                      fit: BoxFit.cover,
                      image: NetworkImage(e),
                    ),
                  ))
              .toList(),
        ),
      ),
    );
  }
}
