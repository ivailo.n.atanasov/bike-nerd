import 'package:auto_size_text/auto_size_text.dart';
import 'package:bikenerd/ads/model/ad.dart';
import 'package:bikenerd/utils/utils.dart';
import 'package:flutter/material.dart';

class AdListItem extends StatelessWidget {
  final String tittle;
  final String coverPictureUrl;
  final double price;
  final String heroTag;
  final AdType adType;

  AdListItem(
      this.tittle, this.coverPictureUrl, this.price, this.heroTag, this.adType);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 6,
      child: Container(
        padding: const EdgeInsets.all(0.2),
        child: ClipRRect(
          borderRadius: BorderRadius.all(Radius.circular(4)),
          child: Stack(
            children: [
              Positioned.fill(
                child: Hero(
                  tag: heroTag,
                  child: FadeInImage.assetNetwork(
                    fit: BoxFit.cover,
                    image: coverPictureUrl,
                    placeholder: 'assets/images/background.png',
                  ),
                ),
              ),
              Positioned(
                left: 0,
                top: 0,
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(2)),
                      color: Theme.of(context).primaryColor),
                  margin: const EdgeInsets.all(2),
                  padding: const EdgeInsets.all(4),
                  child: Text(
                    Utils.getDisplayedPrice(price),
                    style: TextStyle(fontSize: 14, color: Colors.white),
                    softWrap: true,
                    overflow: TextOverflow.fade,
                  ),
                ),
              ),
              Positioned(
                bottom: 0,
                left: 0,
                right: 0,
                child: Container(
                  padding: const EdgeInsets.all(8),
                  color: Colors.black54,
                  child: AutoSizeText(
                    tittle,
                    style: TextStyle(fontSize: 14, color: Colors.white),
                    softWrap: true,
                    overflow: TextOverflow.fade,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class FavoriteIconWidget extends StatefulWidget {
  @override
  _FavoriteIconWidgetState createState() => _FavoriteIconWidgetState();
}

class _FavoriteIconWidgetState extends State<FavoriteIconWidget> {
  bool _isFavorite = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: IconButton(
          icon: _isFavorite
              ? Icon(
                  Icons.favorite,
                  color: Colors.red,
                  size: 35,
                )
              : Icon(
                  Icons.favorite_border,
                  color: Colors.grey,
                  size: 30,
                ),
          onPressed: onIconPressed),
    );
  }

  void onIconPressed() {
    setState(() {
      _isFavorite = !_isFavorite;
    });
  }
}
