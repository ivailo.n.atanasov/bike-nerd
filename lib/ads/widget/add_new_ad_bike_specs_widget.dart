import 'package:bikenerd/ads/model/bike_specs.dart';
import 'package:bikenerd/ads/provider/ad_provider.dart';
import 'package:bikenerd/ressources/strings.dart';
import 'package:bikenerd/utils/ui_utils.dart';
import 'package:bikenerd/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AddNewAdBikeSpecsWidget extends StatefulWidget {
  @override
  _AddNewAdBikeSpecsWidgetState createState() =>
      _AddNewAdBikeSpecsWidgetState();
}

class _AddNewAdBikeSpecsWidgetState extends State<AddNewAdBikeSpecsWidget> {
  bool _isExpanded = false;

  TextEditingController frame = TextEditingController();
  TextEditingController suspension = TextEditingController();
  TextEditingController frontWheel = TextEditingController();
  TextEditingController rearWheel = TextEditingController();
  TextEditingController tires = TextEditingController();
  TextEditingController gears = TextEditingController();
  TextEditingController saddle = TextEditingController();
  TextEditingController handlebar = TextEditingController();
  TextEditingController breaks = TextEditingController();
  TextEditingController battery = TextEditingController();
  TextEditingController weight = TextEditingController();
  TextEditingController productWebsite = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final adProvider = Provider.of<AdProvider>(context, listen: false);
    return Container(
      decoration: UiUtils.generalBoxDecoration(),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            child: GestureDetector(
              onTap: () {
                setState(() {
                  Utils.removeKeyboardFocus(context);
                  _isExpanded = !_isExpanded;
                });
              },
              child: Row(
                children: <Widget>[
                  SizedBox(
                    width: 8,
                  ),
                  Icon(
                    Icons.settings,
                    color: Theme.of(context).primaryColor,
                  ),
                  SizedBox(
                    width: 12,
                  ),
                  Expanded(
                      child: Text(
                    Strings.addBikeSpecs,
                    style: TextStyle(color: Colors.grey),
                  )),
                  SizedBox(
                    width: 8,
                  ),
                  IconButton(
                    icon: _isExpanded
                        ? Icon(Icons.keyboard_arrow_up)
                        : Icon(Icons.keyboard_arrow_down),
                    onPressed: () {
                      setState(() {
                        Utils.removeKeyboardFocus(context);
                        _isExpanded = !_isExpanded;
                      });
                    },
                  ),
                ],
              ),
            ),
          ),
          if (_isExpanded)
            Container(
              padding: const EdgeInsets.all(16),
              color: Colors.grey.withAlpha(40),
              child: Form(
                child: (Column(
                  children: <Widget>[
                    SizedBox(
                      height: 4,
                    ),
                    _buildTitle(Strings.bikeSpecFrameset),
                    _buildFormWidget(adProvider, Strings.bikeSpecsFrame, frame,
                        30, BikeSpecs.keyFrame),
                    _buildFormWidget(adProvider, Strings.bikeSpecsSuspension,
                        suspension, 30, BikeSpecs.keySuspension),
                    _buildTitle(Strings.bikeSpecWheels),
                    _buildFormWidget(adProvider, Strings.bikeSpecsFrontWheel,
                        frontWheel, 30, BikeSpecs.keyFrontWheel),
                    _buildFormWidget(adProvider, Strings.bikeSpecsRearWheel,
                        rearWheel, 30, BikeSpecs.keyRearWheel),
                    _buildFormWidget(adProvider, Strings.bikeSpecsTires, tires,
                        30, BikeSpecs.keyTires),
                    _buildTitle(Strings.bikeSpecComponents),
                    _buildFormWidget(adProvider, Strings.bikeSpecksGears, gears,
                        30, BikeSpecs.keyGears),
                    _buildFormWidget(adProvider, Strings.bikeSpecksSaddle,
                        saddle, 30, BikeSpecs.keySaddle),
                    _buildFormWidget(adProvider, Strings.bikeSpecksHandleBar,
                        handlebar, 30, BikeSpecs.keyHandlebar),
                    _buildFormWidget(adProvider, Strings.bikeSpecksBreaks,
                        breaks, 30, BikeSpecs.keyBreaks),
                    _buildFormWidget(adProvider, Strings.bikeSpecksBattery,
                        battery, 30, BikeSpecs.keyBattery),
                    _buildTitle(Strings.bikeSpecsGeneral),
                    _buildFormWidget(adProvider, Strings.bikeSpecksWeight,
                        weight, 10, BikeSpecs.keyWeight),
                    _buildFormWidget(adProvider, Strings.bikeSpecsWebsite,
                        productWebsite, 50, BikeSpecs.keyProductWebsite),
                  ],
                )),
              ),
            )
        ],
      ),
    );
  }

  @override
  void dispose() {
    frame.dispose();
    suspension.dispose();
    frontWheel.dispose();
    rearWheel.dispose();
    tires.dispose();
    gears.dispose();
    saddle.dispose();
    handlebar.dispose();
    battery.dispose();
    breaks.dispose();
    weight.dispose();
    productWebsite.dispose();
    super.dispose();
  }

  Widget _buildFormWidget(AdProvider adProvider, String hint,
      TextEditingController controller, int maxLength, String key) {
    return TextFormField(
      controller: controller,
      maxLength: maxLength,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
        labelText: hint,
      ),
      onChanged: (value) {
        adProvider.bikeSpecs.addItem(key, value);
      },
    );
  }

  Widget _buildTitle(String title) {
    return Text(
      title,
      textAlign: TextAlign.center,
      style: TextStyle(fontSize: 18, color: Colors.grey),
    );
  }
}
