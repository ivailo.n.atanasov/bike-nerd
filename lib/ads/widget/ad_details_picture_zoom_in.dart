import 'package:bikenerd/utils/ui_utils.dart';
import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';

class AdDetailsPictureZoom extends StatelessWidget {
  final String url;

  AdDetailsPictureZoom(this.url);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: UiUtils.appBar(context, ''),
      body: Container(
          child: PhotoView(
        imageProvider: NetworkImage(url),
      )),
    );
  }
}
