import 'package:bikenerd/ressources/strings.dart';
import 'package:bikenerd/utils/utils.dart';
import 'package:flutter/material.dart';

class AdDetailsBikeSpecs extends StatefulWidget {
  final Map<String, dynamic> specs;

  AdDetailsBikeSpecs(this.specs);

  @override
  _AdDetailsBikeSpecsState createState() => _AdDetailsBikeSpecsState();
}

class _AdDetailsBikeSpecsState extends State<AdDetailsBikeSpecs> {
  bool _isExpanded = false;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            child: GestureDetector(
              onTap: () {
                setState(() {
                  _isExpanded = !_isExpanded;
                });
              },
              child: Row(
                children: <Widget>[
                  Icon(
                    Icons.settings,
                    color: Theme.of(context).primaryColor,
                  ),
                  SizedBox(
                    width: 12,
                  ),
                  Expanded(
                      child: Text(
                    Strings.bikeSpecs,
                    style: TextStyle(color: Colors.grey),
                  )),
                  SizedBox(
                    width: 8,
                  ),
                  IconButton(
                    icon: _isExpanded
                        ? Icon(Icons.keyboard_arrow_up)
                        : Icon(Icons.keyboard_arrow_down),
                    onPressed: () {
                      setState(() {
                        _isExpanded = !_isExpanded;
                      });
                    },
                  ),
                ],
              ),
            ),
          ),
          if (_isExpanded)
            Container(
              padding: const EdgeInsets.all(16),
              color: Colors.grey.withAlpha(40),
              child: Column(
                children: <Widget>[...getSpecsRows(widget.specs)],
              ),
            )
        ],
      ),
    );
  }

  List<BikeSpecsRowWidget> getSpecsRows(Map<String, dynamic> specs) {
    List<BikeSpecsRowWidget> widgetList = [];
    specs.forEach((key, value) {
      widgetList.add(BikeSpecsRowWidget(key, value));
    });
    return widgetList.reversed.toList();
  }
}

class BikeSpecsRowWidget extends StatelessWidget {
  final String title;
  final String body;

  BikeSpecsRowWidget(this.title, this.body);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            height: 8,
          ),
          Text(
            _capitalizeString(title),
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: 4,
          ),
          title == 'website'
              ? GestureDetector(
                  onTap: () {
                    Utils.sendUrl(
                        context, 'http:$body', Strings.contactErrorBrowser);
                  },
                  child: Text(
                    body,
                    style: TextStyle(
                        decoration: TextDecoration.underline,
                        color: Colors.blue),
                  ))
              : Text(body),
          Divider()
        ],
      ),
    );
  }

  String _capitalizeString(String s) {
    return s[0].toUpperCase() + s.substring(1).replaceAll('_', ' ');
  }
}
