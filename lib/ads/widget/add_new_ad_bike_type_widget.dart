import 'package:bikenerd/ads/model/ad.dart';
import 'package:bikenerd/ads/provider/ad_provider.dart';
import 'package:bikenerd/ressources/strings.dart';
import 'package:bikenerd/utils/ui_utils.dart';
import 'package:bikenerd/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AddNewAdBikeTypeWidget extends StatefulWidget {
  @override
  _AddNewAdBikeTypeWidgetState createState() => _AddNewAdBikeTypeWidgetState();
}

class _AddNewAdBikeTypeWidgetState extends State<AddNewAdBikeTypeWidget> {
  bool _isExpanded = false;
  String _selectedItem;

  @override
  Widget build(BuildContext context) {
    final adProvider = Provider.of<AdProvider>(context, listen: false);
    return Container(
      decoration: UiUtils.generalBoxDecoration(),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            child: GestureDetector(
              onTap: () {
                setState(() {
                  Utils.removeKeyboardFocus(context);
                  _isExpanded = !_isExpanded;
                });
              },
              child: Row(
                children: <Widget>[
                  SizedBox(
                    width: 8,
                  ),
                  Icon(
                    Icons.directions_bike,
                    color: Theme.of(context).primaryColor,
                  ),
                  SizedBox(
                    width: 12,
                  ),
                  Expanded(
                      child: Text(
                    _selectedItem == null ? Strings.addBikeType : _selectedItem,
                    style: TextStyle(
                        color:
                            _selectedItem == null ? Colors.grey : Colors.black),
                  )),
                  SizedBox(
                    width: 8,
                  ),
                  IconButton(
                    icon: _isExpanded
                        ? Icon(Icons.keyboard_arrow_up)
                        : Icon(Icons.keyboard_arrow_down),
                    onPressed: () {
                      setState(() {
                        _isExpanded = !_isExpanded;
                      });
                    },
                  ),
                ],
              ),
            ),
          ),
          if (_isExpanded)
            Container(
              color: Colors.grey.withAlpha(40),
              child: Column(
                children: <Widget>[
                  ListTile(
                    title: Text(Strings.bikeTypeCity),
                    onTap: () => _onItemClicked(
                        Strings.bikeTypeCity, BikeType.CityBike, adProvider),
                  ),
                  Divider(),
                  ListTile(
                    title: Text(Strings.bikeTypeRoad),
                    onTap: () => _onItemClicked(
                        Strings.bikeTypeRoad, BikeType.RoadBike, adProvider),
                  ),
                  Divider(),
                  ListTile(
                    title: Text(Strings.bikeTypeElectric),
                    onTap: () => _onItemClicked(Strings.bikeTypeElectric,
                        BikeType.ElectricBike, adProvider),
                  ),
                  Divider(),
                  ListTile(
                    title: Text(Strings.bikeTypeMountain),
                    onTap: () => _onItemClicked(Strings.bikeTypeMountain,
                        BikeType.MountainBike, adProvider),
                  ),
                  Divider(),
                  ListTile(
                    title: Text(Strings.bikeTypeGravel),
                    onTap: () => _onItemClicked(Strings.bikeTypeGravel,
                        BikeType.GravelBike, adProvider),
                  ),
                  Divider(),
                  ListTile(
                    title: Text(Strings.bikeTypeKids),
                    onTap: () => _onItemClicked(
                        Strings.bikeTypeKids, BikeType.KidsBike, adProvider),
                  ),
                ],
              ),
            )
        ],
      ),
    );
  }

  void _onItemClicked(
      String bikeTypeTitle, BikeType bikeType, AdProvider provider) {
    setState(() {
      Utils.removeKeyboardFocus(context);
      provider.bikeType = bikeType;
      _isExpanded = false;
      _selectedItem = bikeTypeTitle;
    });
  }
}
