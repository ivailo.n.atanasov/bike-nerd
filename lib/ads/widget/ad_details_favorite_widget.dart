import 'package:bikenerd/utils/firebase_api.dart';
import 'package:flutter/material.dart';

class AdDetailsFavoriteWidget extends StatefulWidget {
  final String adId;
  final bool isFavorite;
  final int adType;

  AdDetailsFavoriteWidget(this.adId, this.isFavorite, this.adType);

  @override
  _AdDetailsFavoriteWidgetState createState() =>
      _AdDetailsFavoriteWidgetState(isFavorite);
}

class _AdDetailsFavoriteWidgetState extends State<AdDetailsFavoriteWidget> {
  bool _isFavorite = false;

  _AdDetailsFavoriteWidgetState(this._isFavorite);

  @override
  Widget build(BuildContext context) {
    return Container(
        child: IconButton(
      iconSize: 30,
      color: Theme.of(context).accentColor,
      icon: _isFavorite
          ? Icon(Icons.favorite)
          : Icon(
              Icons.favorite_border,
            ),
      onPressed: () {
        setState(() {
          _isFavorite = !_isFavorite;
          _showSnackBar(context, _isFavorite);
          FirebaseApi.setAdFavorite(_isFavorite, widget.adId, widget.adType);
        });
      },
    ));
  }

  _showSnackBar(BuildContext context, bool isFavorite) {
    Scaffold.of(context).hideCurrentSnackBar();
    String message =
        'Ad was ${isFavorite ? 'added to' : 'removed from'} your favorite';
    Scaffold.of(context).showSnackBar(SnackBar(
      content: Text(message),
      backgroundColor: Theme.of(context).primaryColor,
    ));
  }
}
