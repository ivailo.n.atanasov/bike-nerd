class BikeSpecs {
  static final String keyFrame = 'frame';
  static final String keySuspension = 'suspension';
  static final String keyFrontWheel = 'front_wheel';
  static final String keyRearWheel = 'rear_wheel';
  static final String keyTires = 'tires';
  static final String keyGears = 'gears';
  static final String keySaddle = 'saddle';
  static final String keyHandlebar = 'handlebar';
  static final String keyBreaks = 'breaks';
  static final String keyBattery = 'battery';
  static final String keyWeight = 'weight';
  static final String keyProductWebsite = 'website';

  final Map<String, String> _specs = {};

  void addItem(String key, String value) {
    if (value.trim().isEmpty) {
      _specs.remove(key);
    } else {
      _specs[key] = value;
    }
  }

  Map<String, String> get specs => _specs.length == 0 ? null : _specs;
}
