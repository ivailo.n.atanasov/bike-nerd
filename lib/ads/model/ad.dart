import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class Ad {
  final String creatorId;
  final String title;
  final String description;
  final String contactEmail;
  final String contactPhone;

  final String city;
  final String postalCode;

  final double price;

  final Map<String, dynamic> adPictures;

  final int adType;
  final int bikeType;

  final Timestamp createdAt;

  final Map<String, String> bikeSpecs;

  Ad({
    @required this.creatorId,
    @required this.description,
    @required this.title,
    @required this.contactEmail,
    @required this.price,
    @required this.adType,
    @required this.createdAt,
    @required this.city,
    @required this.postalCode,
    @required this.adPictures,
    this.bikeType,
    this.bikeSpecs,
    this.contactPhone,
  });
}

class AdPicture {
  final String uuid;
  final String url;

  AdPicture(this.uuid, this.url);

  Map<String, String> toMap() {
    return {
      'uuid': uuid,
      'url': url,
    };
  }
}

enum AdType { Bikes, Parts }

enum BikeType {
  MountainBike,
  RoadBike,
  GravelBike,
  CityBike,
  ElectricBike,
  KidsBike,
}
