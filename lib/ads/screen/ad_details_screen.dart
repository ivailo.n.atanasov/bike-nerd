import 'package:bikenerd/ads/model/ad.dart';
import 'package:bikenerd/ads/widget/ad_details_bike_specs_widget.dart';
import 'package:bikenerd/ads/widget/ad_details_contact_widget.dart';
import 'package:bikenerd/ads/widget/ad_details_favorite_widget.dart';
import 'package:bikenerd/ads/widget/ad_details_images_widget.dart';
import 'package:bikenerd/ads/widget/ad_details_info_item.dart';
import 'package:bikenerd/utils/firebase_api.dart';
import 'package:bikenerd/utils/ui_utils.dart';
import 'package:bikenerd/utils/utils.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class AdDetailsScreen extends StatelessWidget {
  static final String routeName = '/ad_details';

  @override
  Widget build(BuildContext context) {
    final DetailsScreenArguments arguments =
        ModalRoute.of(context).settings.arguments as DetailsScreenArguments;
    final DocumentSnapshot document = arguments.document;
    int adType = document['ad_type'];
    return Scaffold(
      appBar: UiUtils.appBar(context, 'Details'),
      body: SingleChildScrollView(
        child: Container(
          width: double.infinity,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              AdDetailsImagesWidget(
                  getPictureUrls(document), arguments.heroTag),
              Container(
                padding: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
                width: double.infinity,
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: Text(document['title'],
                          textAlign: TextAlign.start,
                          style: TextStyle(
                              fontSize: 22,
                              fontWeight: FontWeight.bold,
                              fontFamily: 'Roboto')),
                    ),
                    FutureBuilder(
                      future: isFavorite(document.documentID, adType),
                      initialData: SizedBox(),
                      builder: (context, snapshot) {
                        return snapshot.data;
                      },
                    )
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
                width: double.infinity,
                child: Text(Utils.getDisplayedPrice(document['price']),
                    textAlign: TextAlign.start,
                    style: TextStyle(
                        fontSize: 22,
                        color: Theme.of(context).accentColor,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'Roboto')),
              ),
              AdDetailsContactWidget(document['contact_email'],
                  document['title'], document['contact_phone']),
              Container(
                padding: EdgeInsets.only(top: 16, left: 16, right: 16),
                width: double.infinity,
                child: Text(
                  document['description'],
                  textAlign: TextAlign.start,
                ),
              ),
              SizedBox(
                height: 32,
              ),
              if (document['bike_specs'] != null &&
                  adType != AdType.Parts.index)
                AdDetailsBikeSpecs(document['bike_specs']),
              AdDetailsInfoItemWidget(
                  '${document['city']}, ${document['postal_code']}',
                  Icons.location_on),
              if (adType != AdType.Parts.index)
                AdDetailsInfoItemWidget(
                    Utils.getDisplayedBike(document['bike_type']),
                    Icons.directions_bike),
              SizedBox(
                height: 32,
              ),
            ],
          ),
        ),
      ),
    );
  }

  List<String> getPictureUrls(DocumentSnapshot document) {
    final List<String> urls = [];

    urls.add(document['ad_pictures']['picture_1']['url']);

    if (document['ad_pictures']['picture_2'] != null) {
      urls.add(document['ad_pictures']['picture_2']['url']);
    }

    if (document['ad_pictures']['picture_3'] != null) {
      urls.add(document['ad_pictures']['picture_3']['url']);
    }

    return urls;
  }

  Future<Widget> isFavorite(String adId, int adType) async {
    final user = await FirebaseAuth.instance.currentUser();
    if (user == null) return SizedBox();
    final favorites = await FirebaseApi.getFavoriteAdsId();
    return AdDetailsFavoriteWidget(adId, favorites.contains(adId), adType);
  }
}

class DetailsScreenArguments {
  final DocumentSnapshot document;
  final String heroTag;

  DetailsScreenArguments(this.document, this.heroTag);
}
