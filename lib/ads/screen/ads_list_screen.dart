import 'package:bikenerd/ads/model/ad.dart';
import 'package:bikenerd/ads/screen/ad_details_screen.dart';
import 'package:bikenerd/ads/widget/ad_list_item.dart';
import 'package:bikenerd/ressources/strings.dart';
import 'package:bikenerd/utils/firebase_api.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class AdsListScreen extends StatelessWidget {
  final AdType adsType;

  AdsListScreen(this.adsType);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Padding(
      padding: const EdgeInsets.all(6),
      child: StreamBuilder<QuerySnapshot>(
        stream: Firestore.instance
            .collection(FirebaseApi.getCollectionName(adsType))
            .snapshots(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasError) return new Text('Error: ${snapshot.error}');
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(child: CircularProgressIndicator());
          } else {
            if (snapshot.hasData && snapshot.data.documents.length > 0) {
              return _buildGridView(context, snapshot);
            } else {
              return Center(
                  child: Text(
                Strings.adsNoResultsFound,
                style: TextStyle(color: Colors.grey),
              ));
            }
          }
        },
      ),
    ));
  }

  GridView _buildGridView(
      BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
    return GridView.builder(
        itemCount: snapshot.data.documents.length,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          childAspectRatio: 2 / 2,
          mainAxisSpacing: 6,
          crossAxisSpacing: 2,
        ),
        itemBuilder: (context, index) {
          final document = snapshot.data.documents[index];
          final String heroTag = 'picture-$index';
          return GestureDetector(
            onTap: () {
              Navigator.of(context).pushNamed(AdDetailsScreen.routeName,
                  arguments: DetailsScreenArguments(document, heroTag));
            },
            child: AdListItem(
                document['title'],
                (document['ad_pictures']['picture_1']['url']),
                document['price'],
                heroTag,
                adsType),
          );
        });
  }
}
