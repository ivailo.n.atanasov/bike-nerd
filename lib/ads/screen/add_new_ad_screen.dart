import 'package:bikenerd/ads/provider/ad_provider.dart';
import 'package:bikenerd/ads/widget/add_new_ad_info_form_widget.dart';
import 'package:bikenerd/utils/ui_utils.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AddNewAdScreen extends StatelessWidget {
  static final String routeName = '/add_ad';

  @override
  Widget build(BuildContext context) {
    final adType = ModalRoute.of(context).settings.arguments;
    return ChangeNotifierProvider(
      create: (_) => AdProvider(),
      child: Scaffold(
        appBar: UiUtils.appBar(context, 'New ad'),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(16),
            child: Center(
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 16,
                  ),
                  AdNewAdInfoFormWidget(adType),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
