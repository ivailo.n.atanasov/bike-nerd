import 'dart:io';

import 'package:bikenerd/ads/model/ad.dart';
import 'package:bikenerd/ads/model/bike_specs.dart';
import 'package:bikenerd/utils/firebase_api.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class AdProvider with ChangeNotifier {
  List<File> adPictureFiles = [null, null, null];
  BikeType bikeType;
  BikeSpecs bikeSpecs = BikeSpecs();

  Future<void> publishAd(
      {@required AdType adType,
      @required String title,
      @required String description,
      @required double price,
      @required String email,
      @required String phone,
      @required String city,
      @required String postcode}) async {
    Map<String, dynamic> extraPictures = {};

    for (var i = 0; i < adPictureFiles.length; i++) {
      if (adPictureFiles[i] != null) {
        final result =
            await FirebaseApi.uploadAdPicture(adType, adPictureFiles[i]);
        extraPictures['picture_${i + 1}'] = result.toMap();
      }
    }

    final user = await FirebaseAuth.instance.currentUser();
    final result = await FirebaseApi.uploadAd(
        adType,
        Ad(
            creatorId: user.uid,
            adType: adType.index,
            title: title,
            city: city,
            postalCode: postcode,
            description: description,
            adPictures: extraPictures.isEmpty ? null : extraPictures,
            price: price,
            contactEmail: email,
            contactPhone: phone,
            bikeType: adType == AdType.Parts ? null : bikeType.index,
            bikeSpecs: adType == AdType.Parts ? null : bikeSpecs.specs,
            createdAt: Timestamp.now()));

    FirebaseApi.addToUserAds(result.documentID, adType.index);
  }
}
