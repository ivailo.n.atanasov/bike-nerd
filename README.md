# Bike Nerd
Bike Nerd is a demo mobile application built entirely with Flutter. It supports both Android and iOS. The app uses Firebase integration for user authentication, database and file storage.The data displayed by the app is mocked and used for demo purpose only.

## Features

### Ads

You can browse ads of bikes/parts and insert new ones when logged in as a user.

### Bike Events

You can browse or insert bike events when logged as a user.

[See Demo video here](https://drive.google.com/file/d/1uhADOsTqraVn6mmxczqUkKo15RZKACjz/view?usp=sharing)